package formularios;

import Clases.Despensa;
import Clases.Ingrediente;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class frmDespensa extends javax.swing.JFrame {
    List<Ingrediente> objIngrediente;
    List<Despensa> objDespensa;
    
    public frmDespensa() {
        initComponents();
        this.setLocationRelativeTo(null);//centra la pantalla
        this.setResizable(false);//no permite maximizar o minimizar ventana
    }
    void Limpiar(){
        this.txtIngrediente1.setText("");
        this.txtIngrediente2.setText("");
        this.txtIngrediente3.setText("");
        this.txtIngrediente4.setText("");
        this.txtIngrediente5.setText("");
        this.txtIngrediente6.setText("");
        this.txtIngrediente7.setText("");
        this.txtIngrediente8.setText("");
        this.txtIngrediente9.setText("");
    }
    
    void Validacion(){
        if ("".equals(this.txtIngrediente1.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngrediente2.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngrediente3.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngrediente4.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngrediente5.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngrediente6.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngrediente7.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngrediente8.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngrediente9.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
    }
     
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        lblsubtitulo1 = new javax.swing.JLabel();
        lblIngrediente1 = new javax.swing.JLabel();
        lblIngrediente2 = new javax.swing.JLabel();
        lblIngrediente3 = new javax.swing.JLabel();
        lblIngrediente4 = new javax.swing.JLabel();
        lblIngrediente5 = new javax.swing.JLabel();
        txtIngrediente1 = new javax.swing.JTextField();
        txtIngrediente2 = new javax.swing.JTextField();
        txtIngrediente3 = new javax.swing.JTextField();
        txtIngrediente4 = new javax.swing.JTextField();
        txtIngrediente5 = new javax.swing.JTextField();
        lblIngrediente6 = new javax.swing.JLabel();
        txtIngrediente6 = new javax.swing.JTextField();
        lblIngrediente7 = new javax.swing.JLabel();
        lblIngrediente8 = new javax.swing.JLabel();
        lblIngrediente9 = new javax.swing.JLabel();
        txtIngrediente7 = new javax.swing.JTextField();
        txtIngrediente8 = new javax.swing.JTextField();
        txtIngrediente9 = new javax.swing.JTextField();
        btnLimpiar = new javax.swing.JButton();
        btnRegresar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 102, 255));
        jPanel1.setToolTipText("");

        lblTitulo.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(255, 255, 255));
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("DESPENSA");
        lblTitulo.setToolTipText("");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(206, 206, 206)
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(lblTitulo)
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        lblsubtitulo1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblsubtitulo1.setText("INGREDIENTES");

        lblIngrediente1.setText("PISCO SOUR");
        lblIngrediente1.setToolTipText("");

        lblIngrediente2.setText("LIMÓN");

        lblIngrediente3.setText("RON");
        lblIngrediente3.setToolTipText("");

        lblIngrediente4.setText("JARABE DE GOMA");
        lblIngrediente4.setToolTipText("");

        lblIngrediente5.setText("GINGER");
        lblIngrediente5.setToolTipText("");

        lblIngrediente6.setText("GASEOSA NEGRA");
        lblIngrediente6.setToolTipText("");

        lblIngrediente7.setText("AGUA CON GAS");
        lblIngrediente7.setToolTipText("");

        lblIngrediente8.setText("FRESA");

        lblIngrediente9.setText("PIÑA");

        btnLimpiar.setText("LIMPIAR");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnRegresar.setText("REGRESAR");
        btnRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegresarActionPerformed(evt);
            }
        });

        btnMostrar.setText("MOSTRAR");
        btnMostrar.setEnabled(false);
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });

        btnGuardar.setText("GUARDAR");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblsubtitulo1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtIngrediente1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lblIngrediente4)
                                        .addComponent(lblIngrediente5)
                                        .addComponent(lblIngrediente3)
                                        .addComponent(lblIngrediente2)
                                        .addComponent(lblIngrediente1))
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                            .addGap(9, 9, 9)
                                            .addComponent(txtIngrediente2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(txtIngrediente3, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                                                .addComponent(txtIngrediente4)))))
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(btnRegresar)
                                    .addGap(73, 73, 73)
                                    .addComponent(btnGuardar))
                                .addComponent(txtIngrediente5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(95, 95, 95)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblIngrediente7)
                                    .addComponent(lblIngrediente8)
                                    .addComponent(lblIngrediente9)
                                    .addComponent(lblIngrediente6))
                                .addGap(15, 15, 15)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtIngrediente6, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtIngrediente8)
                                        .addComponent(txtIngrediente9)
                                        .addComponent(txtIngrediente7, javax.swing.GroupLayout.DEFAULT_SIZE, 63, Short.MAX_VALUE))))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(72, 72, 72)
                                .addComponent(btnMostrar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnLimpiar)))))
                .addContainerGap(107, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addComponent(lblsubtitulo1)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIngrediente1)
                    .addComponent(txtIngrediente1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIngrediente6)
                    .addComponent(txtIngrediente6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblIngrediente2)
                            .addComponent(txtIngrediente2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblIngrediente3)
                            .addComponent(txtIngrediente3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblIngrediente4)
                            .addComponent(txtIngrediente4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblIngrediente7)
                            .addComponent(txtIngrediente7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(13, 13, 13)
                                .addComponent(lblIngrediente8)
                                .addGap(18, 18, 18)
                                .addComponent(lblIngrediente9))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtIngrediente8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtIngrediente9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIngrediente5)
                    .addComponent(txtIngrediente5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLimpiar)
                    .addComponent(btnRegresar)
                    .addComponent(btnMostrar)
                    .addComponent(btnGuardar))
                .addGap(33, 33, 33))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void btnRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegresarActionPerformed
        frmInicio ini = new frmInicio();
        ini.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnRegresarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        this.txtIngrediente1.setText(String.valueOf(objDespensa.get(0).getCantidadIngredientes())); 
        this.txtIngrediente2.setText(String.valueOf(objDespensa.get(1).getCantidadIngredientes()));
        this.txtIngrediente3.setText(String.valueOf(objDespensa.get(2).getCantidadIngredientes()));
        this.txtIngrediente4.setText(String.valueOf(objDespensa.get(3).getCantidadIngredientes()));
        this.txtIngrediente5.setText(String.valueOf(objDespensa.get(4).getCantidadIngredientes()));
        this.txtIngrediente6.setText(String.valueOf(objDespensa.get(5).getCantidadIngredientes()));
        this.txtIngrediente7.setText(String.valueOf(objDespensa.get(6).getCantidadIngredientes()));
        this.txtIngrediente8.setText(String.valueOf(objDespensa.get(7).getCantidadIngredientes()));
        this.txtIngrediente9.setText(String.valueOf(objDespensa.get(8).getCantidadIngredientes()));
        
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        Validacion();
        
        objIngrediente = new ArrayList<>();
        objDespensa = new ArrayList<>();
        
        objIngrediente.add(new Ingrediente(this.lblIngrediente1.getText()));
        objIngrediente.add(new Ingrediente(this.lblIngrediente2.getText()));
        objIngrediente.add(new Ingrediente(this.lblIngrediente3.getText()));
        objIngrediente.add(new Ingrediente(this.lblIngrediente4.getText()));
        objIngrediente.add(new Ingrediente(this.lblIngrediente5.getText()));
        objIngrediente.add(new Ingrediente(this.lblIngrediente6.getText()));
        objIngrediente.add(new Ingrediente(this.lblIngrediente7.getText()));
        objIngrediente.add(new Ingrediente(this.lblIngrediente8.getText()));
        objIngrediente.add(new Ingrediente(this.lblIngrediente9.getText()));
        
        objDespensa.add(new Despensa(this.lblIngrediente1.getText(), Double.parseDouble(this.txtIngrediente1.getText())));
        objDespensa.add(new Despensa(this.lblIngrediente2.getText(), Double.parseDouble(this.txtIngrediente2.getText())));
        objDespensa.add(new Despensa(this.lblIngrediente3.getText(), Double.parseDouble(this.txtIngrediente3.getText())));
        objDespensa.add(new Despensa(this.lblIngrediente4.getText(), Double.parseDouble(this.txtIngrediente4.getText())));
        objDespensa.add(new Despensa(this.lblIngrediente5.getText(), Double.parseDouble(this.txtIngrediente5.getText())));
        objDespensa.add(new Despensa(this.lblIngrediente6.getText(), Double.parseDouble(this.txtIngrediente6.getText())));
        objDespensa.add(new Despensa(this.lblIngrediente7.getText(), Double.parseDouble(this.txtIngrediente7.getText())));
        objDespensa.add(new Despensa(this.lblIngrediente8.getText(), Double.parseDouble(this.txtIngrediente8.getText())));
        objDespensa.add(new Despensa(this.lblIngrediente9.getText(), Double.parseDouble(this.txtIngrediente9.getText())));
        Limpiar();
        
        this.btnMostrar.setEnabled(true);
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        Limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmDespensa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmDespensa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmDespensa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmDespensa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmDespensa().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnRegresar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblIngrediente1;
    private javax.swing.JLabel lblIngrediente2;
    private javax.swing.JLabel lblIngrediente3;
    private javax.swing.JLabel lblIngrediente4;
    private javax.swing.JLabel lblIngrediente5;
    private javax.swing.JLabel lblIngrediente6;
    private javax.swing.JLabel lblIngrediente7;
    private javax.swing.JLabel lblIngrediente8;
    private javax.swing.JLabel lblIngrediente9;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblsubtitulo1;
    private javax.swing.JTextField txtIngrediente1;
    private javax.swing.JTextField txtIngrediente2;
    private javax.swing.JTextField txtIngrediente3;
    private javax.swing.JTextField txtIngrediente4;
    private javax.swing.JTextField txtIngrediente5;
    private javax.swing.JTextField txtIngrediente6;
    private javax.swing.JTextField txtIngrediente7;
    private javax.swing.JTextField txtIngrediente8;
    private javax.swing.JTextField txtIngrediente9;
    // End of variables declaration//GEN-END:variables
}
