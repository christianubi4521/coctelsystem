package formularios;//paquete formularios

import Clases.Bebida;
import Clases.Coctel;
import Clases.Receta;
import Clases.Ventas;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class frmVentas extends javax.swing.JFrame {
    DecimalFormat df = new DecimalFormat("#.##");//formato de 2 decimales
    List<Ventas> objVentas = new ArrayList<>();;
    List<Coctel> objCoctel;
    Bebida objBebida;
   
    public frmVentas() {
        initComponents();
        this.setResizable(false);//no permite maximizar o minimizar ventana
        this.setLocationRelativeTo(null);//centra la pantalla
        this.txtNroVenta.setText(String.valueOf(this.objVentas.size()+1));
    }

    void Limpiar() {
        this.ckbCoctel1.setSelected(false);
        this.ckbCoctel2.setSelected(false);
        this.ckbCoctel3.setSelected(false);
        this.ckbCoctel4.setSelected(false);
        this.ckbCoctel5.setSelected(false);
        this.ckbCoctel6.setSelected(false);
        this.txtCoctel1.setText("");
        this.txtCoctel2.setText("");
        this.txtCoctel3.setText("");
        this.txtCoctel4.setText("");
        this.txtCoctel5.setText("");
        this.txtCoctel6.setText("");
        this.txtCantidadVendida.setText("");
        this.txtNroVenta.setText(String.valueOf(this.objVentas.size()+1));
    }

    public void Desabilitar() {
        txtCantidadVendida.setEditable(false);
        txtNroVenta.setEditable(false);
    }
    
    void Validacion(){
        if ("".equals(this.txtNroVenta.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Dia ingresado incorrecto", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtCantidadVendida.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public int BuscarCoctel(){
        return 0;
    }
    
    public Double CalcularInsumo(){
        Double total = 0.0;
        objBebida = new Coctel();
        if(this.ckbCoctel1.isSelected()){
            total = objBebida.calcularInsumoCoctel(this.ckbCoctel1.getText()) * Double.valueOf(this.txtCoctel1.getText());
        }
        if(this.ckbCoctel2.isSelected()){
            total = objBebida.calcularInsumoCoctel(this.ckbCoctel2.getText()) * Double.valueOf(this.txtCoctel2.getText());
        }
        if(this.ckbCoctel3.isSelected()){
            total = objBebida.calcularInsumoCoctel(this.ckbCoctel3.getText()) * Double.valueOf(this.txtCoctel3.getText());
        }
        if(this.ckbCoctel4.isSelected()){
            total = objBebida.calcularInsumoCoctel(this.ckbCoctel4.getText()) * Double.valueOf(this.txtCoctel4.getText());
        }
        if(this.ckbCoctel5.isSelected()){
            total = objBebida.calcularInsumoCoctel(this.ckbCoctel5.getText()) * Double.valueOf(this.txtCoctel5.getText());
        }
        if(this.ckbCoctel6.isSelected()){
            total = objBebida.calcularInsumoCoctel(this.ckbCoctel6.getText()) * Double.valueOf(this.txtCoctel6.getText());
        }
        return total;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        objetos = new javax.swing.ButtonGroup();
        jLabel25 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lblCoctel = new javax.swing.JLabel();
        lblDia = new javax.swing.JLabel();
        txtCantidadVendida = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();
        txtNroVenta = new javax.swing.JTextField();
        btnCalcular = new javax.swing.JButton();
        btnRegresar = new javax.swing.JButton();
        lblTotal = new javax.swing.JLabel();
        ckbCoctel1 = new javax.swing.JCheckBox();
        ckbCoctel2 = new javax.swing.JCheckBox();
        ckbCoctel3 = new javax.swing.JCheckBox();
        ckbCoctel4 = new javax.swing.JCheckBox();
        ckbCoctel5 = new javax.swing.JCheckBox();
        ckbCoctel6 = new javax.swing.JCheckBox();
        txtCoctel1 = new javax.swing.JTextField();
        txtCoctel2 = new javax.swing.JTextField();
        txtCoctel3 = new javax.swing.JTextField();
        txtCoctel4 = new javax.swing.JTextField();
        txtCoctel5 = new javax.swing.JTextField();
        txtCoctel6 = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();

        jLabel25.setText("jLabel25");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setForeground(new java.awt.Color(204, 204, 204));
        jPanel1.setPreferredSize(new java.awt.Dimension(680, 500));

        lblCoctel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblCoctel.setText("BEBIDA");
        lblCoctel.setToolTipText("");
        lblCoctel.setPreferredSize(new java.awt.Dimension(200, 25));

        lblDia.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblDia.setText("DIA DE VENTA");
        lblDia.setToolTipText("");
        lblDia.setPreferredSize(new java.awt.Dimension(200, 25));

        txtCantidadVendida.setPreferredSize(new java.awt.Dimension(100, 25));

        btnGuardar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnGuardar.setText("GUARDAR");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCalcular.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnCalcular.setText("CALCULAR");
        btnCalcular.setToolTipText("");
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });

        btnRegresar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRegresar.setText("REGRESAR");
        btnRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegresarActionPerformed(evt);
            }
        });

        lblTotal.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblTotal.setText("CANTIDAD VENDIDA");
        lblTotal.setToolTipText("");
        lblTotal.setPreferredSize(new java.awt.Dimension(200, 25));

        ckbCoctel1.setText("PISCO SOUR");
        ckbCoctel1.setToolTipText("");

        ckbCoctel2.setText("CHILCANO");

        ckbCoctel3.setText("MOJITO");

        ckbCoctel4.setText("DAIQUIRI");

        ckbCoctel5.setText("PIÑA COLADA");
        ckbCoctel5.setToolTipText("");

        ckbCoctel6.setText("CUBA LIBRE");

        txtCoctel1.setToolTipText("");

        txtCoctel3.setToolTipText("");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(65, 65, 65)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCoctel, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(ckbCoctel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(ckbCoctel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(ckbCoctel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGap(41, 41, 41)
                                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                    .addComponent(lblDia, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(txtNroVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(74, 74, 74))
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(txtCoctel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE)
                                                        .addComponent(txtCoctel2, javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(txtCoctel1, javax.swing.GroupLayout.Alignment.LEADING))
                                                    .addGap(73, 73, 73)
                                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(ckbCoctel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(ckbCoctel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(ckbCoctel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                    .addGap(39, 39, 39))))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                            .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(txtCantidadVendida, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(btnCalcular)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtCoctel4, javax.swing.GroupLayout.DEFAULT_SIZE, 63, Short.MAX_VALUE)
                                        .addComponent(txtCoctel5)
                                        .addComponent(txtCoctel6))))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addComponent(btnRegresar, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(78, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(lblCoctel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblDia, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNroVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(txtCoctel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(ckbCoctel1)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(ckbCoctel2)
                                    .addComponent(txtCoctel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ckbCoctel5)
                                    .addComponent(txtCoctel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(22, 22, 22)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ckbCoctel3)
                            .addComponent(txtCoctel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ckbCoctel6)
                            .addComponent(txtCoctel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCantidadVendida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCalcular, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(25, 25, 25)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnRegresar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(33, 33, 33))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ckbCoctel4)
                            .addComponent(txtCoctel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jPanel3.setBackground(new java.awt.Color(0, 102, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel3.setPreferredSize(new java.awt.Dimension(680, 60));

        lblTitulo.setBackground(new java.awt.Color(0, 102, 255));
        lblTitulo.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(255, 255, 255));
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("VENTAS");
        lblTitulo.setPreferredSize(new java.awt.Dimension(640, 30));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 602, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(68, 68, 68))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 602, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 602, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegresarActionPerformed
        // TODO add your handling code here:
        frmInicio ini = new frmInicio();
        ini.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnRegresarActionPerformed

    private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
        // TODO add your handling code here:
        int total = 0;
        if(this.ckbCoctel1.isSelected()){
            total = total + Integer.parseInt(this.txtCoctel1.getText());
        }
         if(this.ckbCoctel2.isSelected()){
            total = total + Integer.parseInt(this.txtCoctel2.getText());
        }
        if(this.ckbCoctel3.isSelected()){
            total = total + Integer.parseInt(this.txtCoctel3.getText());
        }
        if(this.ckbCoctel4.isSelected()){
            total = total + Integer.parseInt(this.txtCoctel4.getText());
        }
        if(this.ckbCoctel5.isSelected()){
            total = total + Integer.parseInt(this.txtCoctel5.getText());
        }
        if(this.ckbCoctel6.isSelected()){
            total = total + Integer.parseInt(this.txtCoctel6.getText());     
        }
        this.txtCantidadVendida.setText(String.valueOf(total));
    }//GEN-LAST:event_btnCalcularActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        //Validacion();
        objCoctel = new ArrayList<>();
        if(this.ckbCoctel1.isSelected()){
            objCoctel.add(new Coctel(this.ckbCoctel1.getText(), Integer.parseInt(this.txtCoctel1.getText()), CalcularInsumo()));
        }
        if(this.ckbCoctel2.isSelected()){
            objCoctel.add(new Coctel(this.ckbCoctel2.getText(), Integer.parseInt(this.txtCoctel2.getText()), CalcularInsumo()));
        }
        if(this.ckbCoctel3.isSelected()){
            objCoctel.add(new Coctel(this.ckbCoctel3.getText(), Integer.parseInt(this.txtCoctel3.getText()), CalcularInsumo()));
        }
        if(this.ckbCoctel4.isSelected()){
            objCoctel.add(new Coctel(this.ckbCoctel4.getText(), Integer.parseInt(this.txtCoctel4.getText()), CalcularInsumo()));
        }
        if(this.ckbCoctel5.isSelected()){
            objCoctel.add(new Coctel(this.ckbCoctel5.getText(), Integer.parseInt(this.txtCoctel5.getText()), CalcularInsumo()));
        }
        if(this.ckbCoctel6.isSelected()){
            objCoctel.add(new Coctel(this.ckbCoctel6.getText(), Integer.parseInt(this.txtCoctel6.getText()), CalcularInsumo()));
        }
        objVentas.add(new Ventas(Integer.parseInt(this.txtNroVenta.getText()), Integer.parseInt(this.txtCantidadVendida.getText()),0.0,this.objCoctel));
        Limpiar();
    }//GEN-LAST:event_btnGuardarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmVentas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnRegresar;
    private javax.swing.JCheckBox ckbCoctel1;
    private javax.swing.JCheckBox ckbCoctel2;
    private javax.swing.JCheckBox ckbCoctel3;
    private javax.swing.JCheckBox ckbCoctel4;
    private javax.swing.JCheckBox ckbCoctel5;
    private javax.swing.JCheckBox ckbCoctel6;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblCoctel;
    private javax.swing.JLabel lblDia;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JLabel lblTotal;
    private javax.swing.ButtonGroup objetos;
    private javax.swing.JTextField txtCantidadVendida;
    private javax.swing.JTextField txtCoctel1;
    private javax.swing.JTextField txtCoctel2;
    private javax.swing.JTextField txtCoctel3;
    private javax.swing.JTextField txtCoctel4;
    private javax.swing.JTextField txtCoctel5;
    private javax.swing.JTextField txtCoctel6;
    private javax.swing.JTextField txtNroVenta;
    // End of variables declaration//GEN-END:variables
}
