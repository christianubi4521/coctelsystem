package formularios;

import Clases.Receta;
import Clases.Ingrediente;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class frmReceta extends javax.swing.JFrame {
 
    public frmReceta() {
        initComponents();
        this.setLocationRelativeTo(null);//centra la pantalla
        this.setResizable(false);//no permite maximizar o minimizar ventana
    } 
    
    List<Receta> objReceta = new ArrayList<>();
    List<Ingrediente> objIngrediente;
    
    void Limpiar(){
        this.txtIngredientes1.setText("");
        this.txtIngredientes2.setText("");
        this.txtIngredientes3.setText("");
        this.txtIngredientes4.setText("");
        this.txtIngredientes5.setText("");
        this.txtIngredientes6.setText("");
        this.txtIngredientes7.setText("");
        this.txtIngredientes8.setText("");
        this.txtIngredientes9.setText("");
        this.txtIngredientes10.setText("");
        this.txtPreparacion.setText("");
    }
    void Validacion(){
        if ("".equals(this.txtIngredientes1.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngredientes2.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngredientes3.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngredientes4.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngredientes5.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngredientes6.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngredientes7.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngredientes8.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngredientes9.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
        if ("".equals(this.txtIngredientes10.getText().trim())){
            JOptionPane.showMessageDialog(null, "Error", "Cantidad ingresada incorrecta", JOptionPane.ERROR_MESSAGE);
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        objetos = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblIngredientes1 = new javax.swing.JLabel();
        lblIngredientes2 = new javax.swing.JLabel();
        lblIngredientes3 = new javax.swing.JLabel();
        lblIngredientes4 = new javax.swing.JLabel();
        txtIngredientes6 = new javax.swing.JTextField();
        txtIngredientes2 = new javax.swing.JTextField();
        txtIngredientes3 = new javax.swing.JTextField();
        txtIngredientes4 = new javax.swing.JTextField();
        lblIngredientes5 = new javax.swing.JLabel();
        lblIngredientes6 = new javax.swing.JLabel();
        txtIngredientes5 = new javax.swing.JTextField();
        lblIngredientes8 = new javax.swing.JLabel();
        lblIngredientes7 = new javax.swing.JLabel();
        txtIngredientes1 = new javax.swing.JTextField();
        lblIngredientes9 = new javax.swing.JLabel();
        lblIngredientes10 = new javax.swing.JLabel();
        txtIngredientes8 = new javax.swing.JTextField();
        txtIngredientes9 = new javax.swing.JTextField();
        txtIngredientes7 = new javax.swing.JTextField();
        txtIngredientes10 = new javax.swing.JTextField();
        btnMostrar = new javax.swing.JButton();
        cboBebida = new javax.swing.JComboBox<>();
        btnRegresar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtPreparacion = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setForeground(new java.awt.Color(204, 204, 204));
        jPanel1.setPreferredSize(new java.awt.Dimension(680, 500));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("SELECCIONE UNA BEBIDA");
        jLabel5.setPreferredSize(new java.awt.Dimension(200, 25));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("INSUMO DE LA BEBIDA");
        jLabel6.setPreferredSize(new java.awt.Dimension(200, 25));

        lblIngredientes1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblIngredientes1.setText("PISCO PURO (ML)");
        lblIngredientes1.setPreferredSize(new java.awt.Dimension(200, 25));

        lblIngredientes2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblIngredientes2.setText("JUGO DE LIMON (ML)");
        lblIngredientes2.setPreferredSize(new java.awt.Dimension(200, 25));

        lblIngredientes3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblIngredientes3.setText("RON BLANCO (ML)");
        lblIngredientes3.setToolTipText("");
        lblIngredientes3.setPreferredSize(new java.awt.Dimension(200, 25));

        lblIngredientes4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblIngredientes4.setText("CUBOS DE HIELO");
        lblIngredientes4.setPreferredSize(new java.awt.Dimension(200, 25));

        txtIngredientes6.setPreferredSize(new java.awt.Dimension(100, 25));

        txtIngredientes2.setPreferredSize(new java.awt.Dimension(100, 25));

        txtIngredientes3.setPreferredSize(new java.awt.Dimension(100, 25));

        txtIngredientes4.setPreferredSize(new java.awt.Dimension(100, 25));

        lblIngredientes5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblIngredientes5.setText("JARABE DE GOMA (ML)");

        lblIngredientes6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblIngredientes6.setText("GINGER (ML)");

        lblIngredientes8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblIngredientes8.setText("GASEOSA NEGRA");

        lblIngredientes7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblIngredientes7.setText("AGUA CON GAS (ML)");

        lblIngredientes9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblIngredientes9.setText("JUGO DE FRESA");

        lblIngredientes10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblIngredientes10.setText("JUGO DE PIÑA");

        btnMostrar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnMostrar.setText("MOSTRAR");
        btnMostrar.setEnabled(false);
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });

        cboBebida.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cboBebida.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pisco Sour", "Chilcano", "Mojito", "Daiquiri", "Piña Colada", "Cuba Libre" }));

        btnRegresar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRegresar.setText("REGRESAR");
        btnRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegresarActionPerformed(evt);
            }
        });

        btnGuardar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnGuardar.setText("GUARDAR");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        txtPreparacion.setColumns(20);
        txtPreparacion.setRows(5);
        jScrollPane1.setViewportView(txtPreparacion);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("PREPARACIÓN");
        jLabel2.setToolTipText("");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 473, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(38, 38, 38)
                                        .addComponent(cboBebida, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(lblIngredientes1, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(59, 59, 59)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(txtIngredientes1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(69, 69, 69)
                                                .addComponent(lblIngredientes6, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(52, 52, 52)
                                                .addComponent(txtIngredientes6, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(149, 149, 149)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(lblIngredientes8, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(lblIngredientes9, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(lblIngredientes10, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGap(55, 55, 55)
                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(txtIngredientes9, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(txtIngredientes8, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(txtIngredientes10, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addComponent(lblIngredientes7, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(35, 35, 35)
                                                        .addComponent(txtIngredientes7, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(btnGuardar)
                                        .addGap(152, 152, 152)
                                        .addComponent(btnMostrar))))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblIngredientes2, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(59, 59, 59)
                                        .addComponent(txtIngredientes2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblIngredientes3, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(lblIngredientes4, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(lblIngredientes5, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel2))
                                        .addGap(59, 59, 59)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtIngredientes4, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtIngredientes3, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtIngredientes5, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGap(38, 38, 38)
                                .addComponent(btnRegresar)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cboBebida)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIngredientes1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIngredientes1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIngredientes6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIngredientes6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIngredientes2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIngredientes2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIngredientes7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIngredientes7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIngredientes3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIngredientes3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIngredientes8, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIngredientes8, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIngredientes4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIngredientes4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIngredientes9, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIngredientes9, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIngredientes5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIngredientes5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIngredientes10, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIngredientes10, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnMostrar)
                            .addComponent(btnRegresar)
                            .addComponent(btnGuardar)))
                    .addComponent(jLabel2))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(0, 102, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel3.setPreferredSize(new java.awt.Dimension(680, 60));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("RECETA");
        jLabel1.setPreferredSize(new java.awt.Dimension(640, 30));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 589, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 682, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 682, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        int n;
        n = this.cboBebida.getSelectedIndex();
        Boolean encontrado = false;
        for(int i = 0; i < this.objReceta.size(); i++){
            if(this.cboBebida.getItemAt(n).equals(this.objReceta.get(i).getNombreReceta())){
                this.txtIngredientes1.setText(String.valueOf(objReceta.get(n).getLstIngredientes().get(0).getCantidad()));
                this.txtIngredientes2.setText(String.valueOf(objReceta.get(n).getLstIngredientes().get(1).getCantidad()));
                this.txtIngredientes3.setText(String.valueOf(objReceta.get(n).getLstIngredientes().get(2).getCantidad()));
                this.txtIngredientes4.setText(String.valueOf(objReceta.get(n).getLstIngredientes().get(3).getCantidad()));
                this.txtIngredientes5.setText(String.valueOf(objReceta.get(n).getLstIngredientes().get(4).getCantidad()));
                this.txtIngredientes6.setText(String.valueOf(objReceta.get(n).getLstIngredientes().get(5).getCantidad()));
                this.txtIngredientes7.setText(String.valueOf(objReceta.get(n).getLstIngredientes().get(6).getCantidad()));
                this.txtIngredientes8.setText(String.valueOf(objReceta.get(n).getLstIngredientes().get(7).getCantidad()));
                this.txtIngredientes9.setText(String.valueOf(objReceta.get(n).getLstIngredientes().get(8).getCantidad()));
                this.txtIngredientes10.setText(String.valueOf(objReceta.get(n).getLstIngredientes().get(9).getCantidad()));
                this.txtPreparacion.setText(String.valueOf(objReceta.get(n).getPreparacion()));
                encontrado = true;
           }
        }
        if(!encontrado) Limpiar();
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegresarActionPerformed
        frmInicio ini = new frmInicio();
        ini.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnRegresarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        int n;
        n = this.cboBebida.getSelectedIndex();
        objIngrediente = new ArrayList<>();
        objIngrediente.add(new Ingrediente(this.lblIngredientes1.getText(), Double.parseDouble(this.txtIngredientes1.getText())));
        objIngrediente.add(new Ingrediente(this.lblIngredientes2.getText(), Double.parseDouble(this.txtIngredientes2.getText())));
        objIngrediente.add(new Ingrediente(this.lblIngredientes3.getText(), Double.parseDouble(this.txtIngredientes3.getText())));
        objIngrediente.add(new Ingrediente(this.lblIngredientes4.getText(), Double.parseDouble(this.txtIngredientes4.getText())));
        objIngrediente.add(new Ingrediente(this.lblIngredientes5.getText(), Double.parseDouble(this.txtIngredientes5.getText())));
        objIngrediente.add(new Ingrediente(this.lblIngredientes6.getText(), Double.parseDouble(this.txtIngredientes6.getText())));
        objIngrediente.add(new Ingrediente(this.lblIngredientes7.getText(), Double.parseDouble(this.txtIngredientes7.getText())));
        objIngrediente.add(new Ingrediente(this.lblIngredientes8.getText(), Double.parseDouble(this.txtIngredientes8.getText())));
        objIngrediente.add(new Ingrediente(this.lblIngredientes9.getText(), Double.parseDouble(this.txtIngredientes9.getText())));
        objIngrediente.add(new Ingrediente(this.lblIngredientes10.getText(), Double.parseDouble(this.txtIngredientes10.getText())));
        objReceta.add(new Receta(this.cboBebida.getItemAt(n), this.objIngrediente, this.txtPreparacion.getText()));             
        Limpiar();
        this.btnMostrar.setEnabled(true);
    }//GEN-LAST:event_btnGuardarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmReceta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmReceta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmReceta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmReceta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmReceta().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnRegresar;
    private javax.swing.JComboBox<String> cboBebida;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblIngredientes1;
    private javax.swing.JLabel lblIngredientes10;
    private javax.swing.JLabel lblIngredientes2;
    private javax.swing.JLabel lblIngredientes3;
    private javax.swing.JLabel lblIngredientes4;
    private javax.swing.JLabel lblIngredientes5;
    private javax.swing.JLabel lblIngredientes6;
    private javax.swing.JLabel lblIngredientes7;
    private javax.swing.JLabel lblIngredientes8;
    private javax.swing.JLabel lblIngredientes9;
    private javax.swing.ButtonGroup objetos;
    private javax.swing.JTextField txtIngredientes1;
    private javax.swing.JTextField txtIngredientes10;
    private javax.swing.JTextField txtIngredientes2;
    private javax.swing.JTextField txtIngredientes3;
    private javax.swing.JTextField txtIngredientes4;
    private javax.swing.JTextField txtIngredientes5;
    private javax.swing.JTextField txtIngredientes6;
    private javax.swing.JTextField txtIngredientes7;
    private javax.swing.JTextField txtIngredientes8;
    private javax.swing.JTextField txtIngredientes9;
    private javax.swing.JTextArea txtPreparacion;
    // End of variables declaration//GEN-END:variables
}
