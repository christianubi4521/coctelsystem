package Clases;

import java.util.List;
 
public class Receta implements Mantenimiento{
    private String NombreReceta;
    private String Preparacion;
    private Coctel coctelRef;
    private List<Ingrediente> lstIngredientes;

    public Receta() {
    }    
    //public Receta(String NombreReceta, String Preparacion, Double Cantidad, String NombreIngredientes, List<Ingrediente> LstIngredientes) {
    public Receta(String NombreReceta, List<Ingrediente> LstIngredientes, String Preparacion) {
        this.NombreReceta = NombreReceta;
        this.Preparacion = Preparacion;
        this.lstIngredientes = LstIngredientes;
 
    }
    
    public String getNombreReceta() {
        return NombreReceta;
    }

    public void setNombreReceta(String NombreReceta) {
        this.NombreReceta = NombreReceta;
    }

    public String getPreparacion() {
        return Preparacion;
    }

    public void setPreparacion(String Preparacion) {
        this.Preparacion = Preparacion;
    }

    public List<Ingrediente> getLstIngredientes() {
        return lstIngredientes;
    }

    public void setLstIngredientes(List<Ingrediente> lstIngredientes) {
        this.lstIngredientes = lstIngredientes;
    }
 
    public void guardarDatos(){
        
    }
    
    @Override
    public void mostrarDatos(){
        
    }
}
