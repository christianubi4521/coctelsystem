package Clases;

import java.util.List;

public class Proyeccion implements Mantenimiento{
    //Datsos de la proyección
    private int NroDia;
    private int n; //número de datos
    public double a, b; //pendiente y ordenada en el origen
    private List<Ventas> lstVentas;
    
    public Proyeccion(List<Ventas> lstVentas) {
        this.lstVentas = lstVentas;
        n = lstVentas.size(); //número de datos
    }

    public List<Ventas> getLstVentas() {
        return lstVentas;
    }

    public void setLstVentas(List<Ventas> lstVentas) {
        this.lstVentas = lstVentas;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
    //Calcular proteccón
    public Double calcularProyeccion(double x, String nombreCoctel) {
        double pxy, sx, sy, sx2, sy2;
        pxy = sx = sy = sx2 = sy2 = 0.0;
        Integer cantidadVendida;
        Double cantidadInsumo;
        
        for (int i = 0; i < n; i++){
            cantidadVendida = lstVentas.get(i).getLstCoctel().get(0).getCantidadCoctel();
            cantidadInsumo =  this.lstVentas.get(i).getLstCoctel().get(0).getCantidadInsumoCoctel();
            sx += cantidadInsumo;
            sy += cantidadVendida;
            sx2 += cantidadInsumo * cantidadInsumo;
            sy2 += cantidadVendida * cantidadVendida;
            pxy += cantidadInsumo * cantidadVendida;            
        }
        b = (n * pxy - sx * sy)/(n * sx2 - sx * sx);
        a = (sy - b * sx)/n;
        return a + b * x;
    }
    
    @Override
    public void guardarDatos(){    
    }
    
    @Override
    public void mostrarDatos(){
    }
}
