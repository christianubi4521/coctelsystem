package Clases;

import java.util.List;

public class Ventas implements Mantenimiento{
    private int numeroDia;
    private int cantidadVendida;
    private Double cantidadInsumo;
    private List<Coctel> lstCoctel;
    private Proyeccion proyeccionRef;

    public Ventas(int numeroDia, int cantidadVendida, Double cantidadInsumo, List<Coctel> lstCoctel) {
        this.numeroDia = numeroDia;
        this.cantidadVendida = cantidadVendida;
        this.cantidadInsumo = cantidadInsumo;
        this.lstCoctel = lstCoctel;
    }
    
    public Ventas(int numeroDia, List<Coctel> lstCoctel) {
        this.numeroDia = numeroDia;
        this.lstCoctel = lstCoctel;
    }

    public int getNumeroDia() {
        return numeroDia;
    }

    public void setNumeroDia(int numeroDia) {
        this.numeroDia = numeroDia;
    }

    public int getCantidadVendida() {
        return cantidadVendida;
    }

    public void setCantidadVendida(int cantidadVendida) {
        this.cantidadVendida = cantidadVendida;
    }

    public Double getCantidadInsumo() {
        return cantidadInsumo;
    }

    public void setCantidadInsumo(Double cantidadInsumo) {
        this.cantidadInsumo = cantidadInsumo;
    }
        
    public List<Coctel> getLstCoctel() {
        return lstCoctel;
    }

    public void setLstCoctel(List<Coctel> lstCoctel) {
        this.lstCoctel = lstCoctel;
    }

    @Override
    public void guardarDatos(){
        
    }
    
    @Override
    public void mostrarDatos(){
        
    }
}
