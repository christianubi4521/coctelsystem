package Clases;

public class Ingrediente {

    private String nombreIngrediente;
    private Double Cantidad;
    private Receta recetaRef;
    private Despensa depensaRef;

    public Ingrediente(String nombreIngrediente) {
        this.nombreIngrediente = nombreIngrediente;
    }

    public Ingrediente(String nombreIngrediente, Double Cantidad) {
        this.nombreIngrediente = nombreIngrediente;
        this.Cantidad = Cantidad;
    }

    public String getNombreIngrediente() {
        return nombreIngrediente;
    }

    public void setNombreIngrediente(String nombreIngrediente) {
        this.nombreIngrediente = nombreIngrediente;
    }

    public Double getCantidad() {
        return Cantidad;
    }

    public void setCantidad(Double Cantidad) {
        this.Cantidad = Cantidad;
    }
}
