package Clases;

import java.util.List;

public class Despensa implements Mantenimiento{
    private String ingredientes;
    private Double CantidadIngredientes;
    private List<Ingrediente> lstIngredientes;
         
    public Despensa(String ingredientes, Double CantidadIngredientes) {
        this.ingredientes = ingredientes;
        this.CantidadIngredientes = CantidadIngredientes;
    }
        
    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }

    public Double getCantidadIngredientes() {
        return CantidadIngredientes;
    }

    public void setCantidadIngredientes(Double CantidadIngredientes) {
        this.CantidadIngredientes = CantidadIngredientes;
    }
    
    @Override
    public void guardarDatos(){
        
    }
    @Override
    public void mostrarDatos(){
        
    }
}
