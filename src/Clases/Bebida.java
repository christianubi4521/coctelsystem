package Clases;

public abstract class Bebida {
    
    public abstract Double calcularInsumoCoctel(String nombreCoctel);
    
}
