package Clases;

import java.util.List;

public class Coctel extends Bebida{
    
    private String NombreCoctel;
    private int cantidadCoctel;
    private Double cantidadInsumoCoctel;
    private List<Receta> lstReceta;
    private Ventas ventasRef;

    public Coctel() {
    }

    public Coctel(String NombreCoctel, int cantidadCoctel, Double cantidadInsumoCoctel) {
        this.NombreCoctel = NombreCoctel;
        this.cantidadCoctel = cantidadCoctel;
        this.cantidadInsumoCoctel = cantidadInsumoCoctel;
    }

    public String getNombreCoctel() {
        return NombreCoctel;
    }

    public void setNombreCoctel(String NombreCoctel) {
        this.NombreCoctel = NombreCoctel;
    }

    public int getCantidadCoctel() {
        return cantidadCoctel;
    }

    public void setCantidadCoctel(int cantidadCoctel) {
        this.cantidadCoctel = cantidadCoctel;
    }

    public Double getCantidadInsumoCoctel() {
        return cantidadInsumoCoctel;
    }

    public void setCantidadInsumoCoctel(Double cantidadInsumoCoctel) {
        this.cantidadInsumoCoctel = cantidadInsumoCoctel;
    }

    public List<Receta> getLstReceta() {
        return lstReceta;
    }

    public void setLstReceta(List<Receta> lstReceta) {
        this.lstReceta = lstReceta;
    }

    
        
   @Override
   public Double calcularInsumoCoctel(String nombreCoctel){
        Double total = 0.0; 
        for(int i = 0; i < this.lstReceta.size(); i++){
            if(this.lstReceta.get(i).getNombreReceta().equals(nombreCoctel)){
                total = total + this.lstReceta.get(i).getLstIngredientes().get(0).getCantidad();
                total = total + this.lstReceta.get(i).getLstIngredientes().get(1).getCantidad();
                total = total + this.lstReceta.get(i).getLstIngredientes().get(2).getCantidad();
                total = total + this.lstReceta.get(i).getLstIngredientes().get(4).getCantidad();
                total = total + this.lstReceta.get(i).getLstIngredientes().get(5).getCantidad();
                total = total + this.lstReceta.get(i).getLstIngredientes().get(6).getCantidad();
            }
        }
        return total;
    }
   
}
